import pygame, os
from .config import *
from .player import Player
from .world import World
from .enemy import Enemy, BossEnemy, Kurbo, McGregor, BossBody
from .menu import PauseMenu
from .particles import ParticleManeger
from .engine import import_sprite_sheets, dist_sq
from .prompts import PromptHandler


class Game:
    def __init__(self, master):
        self.master = master
        self.master.game = self
        self.screen = pygame.display.get_surface()
        self.player = Player(master)
        self.world = World(master)
        self.pause_menu = PauseMenu(master)
        self.particle_manager = ParticleManeger(master)
        self.paused = False

        self.END_WAIT_TIMER = pygame.event.custom_type()

        self.game_finished = False
        self.end_text = self.master.font.render('Thanks For Playing!', False, 'white')
        self.end_rect = self.end_text.get_rect(center=(W//2, H//2))
        self.end2_text = self.master.font_small.render('Press ESC to Continue', False, 'white')
        self.end2_rect = self.end2_text.get_rect(midbottom=(W//2, H-10))

        self.enemy_grp = pygame.sprite.Group()
        self.master.enemy_grp = self.enemy_grp
        self.hazard_enemy_grp = pygame.sprite.Group()
        self.master.hazard_enemy_grp = self.hazard_enemy_grp
        self.load_enemies()
        self.boss_body = BossBody(master)

        self.prompts = PromptHandler(self.master)

    def draw(self):

        self.world.draw_background()
        self.hazard_enemy_grp.draw(self.screen)
        self.player.draw()
        self.enemy_grp.draw(self.screen)
        self.boss_body.draw()
        self.particle_manager.draw()

        self.world.draw_foreground()
        self.prompts.draw()
        self.world.draw_debug()

    def update(self):
        self.master.debug('FPS:', int(self.master.main.clock.get_fps()))
        self.check_events()
        self.world.update()
        self.player.update()
        self.enemy_grp.update()
        self.hazard_enemy_grp.update()
        self.particle_manager.update()
        self.prompts.update()

    def check_events(self):

        for event in pygame.event.get((pygame.KEYDOWN, self.END_WAIT_TIMER)):
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    if self.game_finished:
                        self.game_finished = False
                    else:
                        self.pause_menu.open()
                        self.paused = True
                if event.key == pygame.K_BACKQUOTE:
                    self.master.debug.on  = not self.master.debug.on
                # if event.key == pygame.K_SPACE: # take screenshot
                #     num = 0
                #     while True:
                #         path = "ss/"+F"{num:4d}"+'.png'
                #         if not os.path.exists(path):
                #             pygame.image.save(self.screen, path)
                #             break
                #         num += 1
            if event.type == self.END_WAIT_TIMER:
                self.game_finished = True

    def run_pause_menu(self):
        self.pause_menu.update()
        self.pause_menu.draw()

    def run(self):

        self.master.music.can_play = not self.paused

        if self.paused:
            self.run_pause_menu()
        elif self.game_finished:
            self.check_events()
            self.screen.fill((47, 11, 66))
            self.screen.blit(self.end_text, self.end_rect)
            self.screen.blit(self.end2_text, self.end2_rect)
        else:
            self.update()
            self.draw()

    def load_enemies(self):
        self.enemy_animations = import_sprite_sheets("graphics/enemies")

        Enemy(self.master, [self.enemy_grp], (785,550), "passive", 0, [0,0,0], "worm")
        Enemy(self.master, [self.enemy_grp], (1030,550), "passive", 0, [0,0,0], "jellyfish")
        Enemy(self.master, [self.enemy_grp], (1320,550), "agg01", 0, [2,0,0], "peepers")

        Enemy(self.master, [self.enemy_grp], (270,805), "passive", 1, [0,0,0], "jellyfish")
        Enemy(self.master, [self.enemy_grp], (270,1220), "agg11", 1, [1,1,0], "peepers")
        
        Enemy(self.master, [self.enemy_grp], (1210,1250), "agg21", 2, [3,1,1], "peepers")

        Enemy(self.master, [self.enemy_grp], (1345,1850), "agg31", 3, [3,3,2], "peepers")

        Enemy(self.master, [self.enemy_grp], (605,1700), "agg4shark", 4, [5,3,3], "swordfish")
        Enemy(self.master, [self.enemy_grp], (1010,1910), "agg4shark", 4, [5,3,3], "swordfish")

        Enemy(self.master, [self.enemy_grp], (165,2620), "agg5peeper", 5, [4,3,2], "peepersV2")
        Enemy(self.master, [self.enemy_grp], (750,2620), "agg5peeper", 5, [4,3,2], "peepersV2")
        Enemy(self.master, [self.enemy_grp], (430,2620), "agg5sly", 5, [5,2,1], "Sly")

        Enemy(self.master, [self.enemy_grp], (1005,3145), "passive", 6, [0,0,0], "worm")
        Enemy(self.master, [self.enemy_grp], (500,3145), "agg6shark", 6, [5,3,3], "swordfish")

        Enemy(self.master, [self.enemy_grp], (330,3645), "agg7peeper", 7, [4,2,0], "peepersV3")
        Enemy(self.master, [self.enemy_grp], (645,3810), "agg7sly", 7, [3,2,2], "Sly")
        Enemy(self.master, [self.enemy_grp], (1215,3610), "agg7sly", 7, [3,2,2], "Sly")

        Enemy(self.master, [self.enemy_grp], (1420,4330), "passive", 8, [0,0,0], "jellyfish")

        Kurbo(self.master, [self.hazard_enemy_grp], (220, 3400), 1, pygame.Rect(50,2960,393,927))
        Kurbo(self.master, [self.hazard_enemy_grp], (370, 4985), 1, pygame.Rect(87,4655,1391,471))

        McGregor(self.master, [self.hazard_enemy_grp], (815, 5000), 0)
        McGregor(self.master, [self.hazard_enemy_grp], (1425, 5030), 0)
        McGregor(self.master, [self.hazard_enemy_grp], (150, 4320), 2)

        BossEnemy(self.master, [self.enemy_grp])
