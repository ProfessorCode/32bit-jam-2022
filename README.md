# Deep Abyss

Deep Abyss is an unforgiving and creepy 2D adventure game. You play as an alien fish whose friend was taken by a sea monster. Dive into the dark depths, devour your deadly enemies to save your friend and try to overcome the trials in this bizarre habitat through your determination.

Your goal is to get bigger and evolve into new forms by hunting other creatures. Do not lose your focus, though, and always be alert to the dangers of the ocean. Conquer this increasingly difficult environment by reaching the final stage of your evolution and beating the sea monster.

This game was made for the [32bit jam 2022](https://itch.io/jam/32bit-jam-2022).


## Installation

You can either get the game from [Itch.io](https://professorcode.itch.io/deep-abyss) or run it from source. You need Python 3.8.10 or higher to run it from source.


```
git clone https://codeberg.org/ProfessorCode/32bit-jam-2022
cd 32bit-jam-2022/
pip install -r requirements.txt
python main.py
```

## License

This game has been licensed under **GPL v3** or later. (Please refer to `LICENSE`.)

- **Source Code** - https://codeberg.org/ProfessorCode/32bit-jam-2022

Most graphics, sound and music assets have been licensed under [**CC BY-SA 4.0**](https://creativecommons.org/licenses/by-sa/4.0/).

## Copyright and Attribution

### Team
- **VantaTree** - Lead Python/Pygame Programmer ([Itch.io](https://vantatree.itch.io/), [GitHub](https://github.com/VantaTree))
- **JohnnyBizKit** - Sound Designer and Music Composer ([Itch.io](https://itch.io/profile/pikachujumbo), [SoundCloud](https://soundcloud.com/johnnybizkity))
- **Joshua Myers (Mossclumps)** - 2D Artist and Graphics Designer ([Itch.io](https://mossclumps.itch.io/), [Website](https://joshuamyersart.wixsite.com/my-site), [Instagram](https://www.instagram.com/marimbamessiah_art/))
- **Christian Flores** - 2D/3D Artist and Level Designer ([LinkTree](https://linktr.ee/christianfloresvargas))
- **Granz214** - Game Writer ([Itch.io](https://granz214.itch.io/))
- **ProfessorCode** - Project Manager and Python Programmer ([Itch.io](https://professorcode.itch.io/), [Mastodon](https://fosstodon.org/@ProfessorCode))

### External
- **PressStart2P** Font - [Google](https://fonts.google.com/specimen/Press+Start+2P)
- **Input Prompts** - [kenney.nl](https://kenney.nl/assets/input-prompts-pixel-16)
- **FloatRect Pygame Implementation** - [StarBucks5](https://gist.github.com/Starbuck5/aa9a45dbab0952da3f22dadd826d9a5f)