import pygame
from .config import *
from .engine import load_pngs

class IntroSceneFiFo:
    "fade-in fade-out cutscene"

    def __init__(self, master) -> None:

        self.master = master
        self.screen = pygame.display.get_surface()

        self.scenes = load_pngs("cutscenes/intro")
        self.current_scene = self.scenes[0]
        self.scene_index = 0
        self.alpha = 0
        self.skip = False

        self.increment = 1
        self.alpha_speed = 8

        self.flash_looping = 0
        self.flash_speed = 0.15

        self.bottom_text = self.master.font.render("Press Space to Continue", False, (255, 255, 255))
        self.bottom_text_rect = self.bottom_text.get_rect(midbottom=(W//2, H-10))
        self.skip_text = self.master.font_small.render('ESCAPE to skip', False, 'white')
        self.skip_shadow = self.master.font_small.render('ESCAPE to skip', False, 'black')
        self.skip_rect = self.skip_text.get_rect(topleft=(5, 5))

        # self.STAY_TIMER = pygame.event.custom_type()
        # self.EVENTS = (self.STAY_TIMER)
        self.EVENTS = (pygame.KEYDOWN)

    def check_events(self):

        for event in pygame.event.get(self.EVENTS):
            if event.type == pygame.KEYDOWN: 
                if event.key == pygame.K_SPACE:
                    self.increment = -1
                
                elif event.key == pygame.K_ESCAPE:
                    self.increment = -1
                    self.skip = True

                # if (i:=self.flash_looping):
                #     self.flash_looping = 0
                #     if i == 1: self.scene_index = 23
                #     if i == 2: self.scene_index = 34
                #     continue


    def draw(self):
        
        self.screen.fill(0x0)
        self.screen.blit(self.current_scene, (0, 0))
        if self.increment == 0:
            self.screen.blit(self.bottom_text, self.bottom_text_rect)
            self.screen.blit(self.skip_shadow, (self.skip_rect.x-2, self.skip_rect.y+2))
            self.screen.blit(self.skip_text, self.skip_rect)

    def update(self):

        try:
            self.current_scene = self.scenes[int(self.scene_index)]
        except IndexError: return True
        self.current_scene.set_alpha(int(self.alpha))

        self.alpha += self.alpha_speed*self.increment *self.master.dt
        if (i:=self.flash_looping):
            self.scene_index += self.flash_speed *self.master.dt
            if i == 1 and self.scene_index >= 24: self.scene_index = 13
            if i == 2 and self.scene_index >= 34: self.scene_index = 24

        if self.alpha >= 256:
            self.increment = 0
            self.alpha = 255
            # pygame.time.set_timer(self.STAY_TIMER, 500, loops=1)
        elif self.alpha < 0:
            if self.skip: return True
            self.alpha = 0
            self.increment = 1
            if (i:=self.flash_looping):
                    self.flash_looping = 0
                    if i == 1: self.scene_index = 23
                    if i == 2: self.scene_index = 34
            else: self.scene_index += 1

        if 13 <= int(self.scene_index) <= 22:
            self.flash_looping = 1
        elif 24 <= int(self.scene_index) <= 33:
            self.flash_looping = 2


    def run(self):

        self.check_events()
        result = self.update()
        self.draw()
        return result


class CutScene:

    def __init__(self, master, type, anim_speed):

        self.master = master
        self.type = type
        self.screen = pygame.display.get_surface()

        self.scenes = load_pngs("cutscenes/" + type)
        if type == 'logo' : [self.scenes.append(self.scenes[-1])  for _ in range(24*3)]
        self.current_scene = self.scenes[0]
        self.scene_index = 0
        self.anim_speed = anim_speed

    def draw(self):
        
        # self.screen.fill(0x0)
        self.screen.blit(self.scene, (0, 0))

    def update(self):

        try:
            self.scene = self.scenes[int(self.scene_index)]
        except IndexError:
            return True

        self.scene_index += self.anim_speed *self.master.dt

    
    def run(self):

        result = self.update()
        self.draw()
        return result
        
