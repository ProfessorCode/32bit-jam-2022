import pygame
from .engine import import_sprite_sheets
from enum import Enum
import random

class ParticleEnum(Enum):

    FLOAT = 0
    FADE = 1

class ParticleManeger:

    def __init__(self, master):
        self.master = master
        master.particle = self
        self.particle_grp = pygame.sprite.Group()
        self.screen = pygame.display.get_surface()
        
        self.animations = {}
        self.animations.update(import_sprite_sheets("graphics/particles"))

    def spawn_blood(self, pos, swallow=False):

        if swallow: anim, speed = "blood_swallow", 0.03
        else: anim, speed = "blood_bite", 0.1

        AnimatedParticle(self.master, [self.particle_grp], pos, self.animations[anim], speed, 0, set())

    def spawn_bubbles(self, pos):

        AnimatedParticle(self.master, [self.particle_grp], pos, self.animations['bubbles'], 0.1, 0, set())

    def spawn_dash(self, player):

        offset = player.direction * player.head_offset

        for i in range(1,6):
            pos = player.bite_rect.center - offset - offset/5*i
            speed = random.choice((0.1, 0.08, 0.12, 0.14))*3

            AnimatedParticle(self.master, [self.particle_grp], pos, self.animations["dash_particles"], speed, player.angle, set())

    def draw(self):
        self.particle_grp.draw(self.screen)

    def update(self):
        self.particle_grp.update()



class Particle(pygame.sprite.Sprite):

    def __init__(self, master, grps, pos, image, duration, types:set) -> None:
        super().__init__(grps)
        self.master = master

        self.pos = pos
        self.image = image
        self.rect = self.image.get_rect(center=self.pos + self.master.world.offset)
        self.duration = duration
        self.types = types

    def update(self):
        pass



class AnimatedParticle(Particle):

    def __init__(self, master, grps, pos, animation, speed, angle, types:set) -> None:

        self.animtion = animation
        self.anim_frame = 0
        self.anim_speed = speed
        self.angle = angle

        super().__init__(master, grps, pos, animation[0], 0, types)

    def update_image(self):

        try:
            self.image = self.animtion[int(self.anim_frame)]
        except IndexError:
            self.kill()
            return
            self.anim_frame = 0
            self.image = self.animtion[0]

        self.anim_frame += self.anim_speed *self.master.dt
        if self.angle:
            self.image = pygame.transform.rotate(self.image,self.angle)
        self.rect = self.image.get_rect(center=self.pos + self.master.world.offset)

    def update(self):

        super().update()
        self.update_image()


class BleedParticleManeger:

    def __init__(self, master, p_grp):

        self.master = master
        self.grp = p_grp

        self.step = 0
        self.step_size = 8

    def update(self):

        self.step -= self.master.dt
        if self.step < 0: self.step = self.step_size
        else: return

        player = self.master.player
        health_perc = 1 - (player.health / player.max_health[player.stage])
        self.step_size = 5 if health_perc > 0.8 else 8

        if health_perc > 0.3:
            for _ in range(int((health_perc*15)*self.master.dt)):
                BleedParticle(self.master, [self.grp], player)


class BleedParticle(pygame.sprite.Sprite):

    def __init__(self, master, grps, player):

        self.master = master
        super().__init__(grps)
        self.player = player

        self.pos = pygame.Vector2(player.bite_rect.center) + random.random()*10*(-player.direction)
        self.speed = 2
        self.decel = 0.1
        self.direction = -player.direction.rotate(random.randint(-8,8))
    
        self.alpha = 255
        self.alpha_speed = 5

        self.stopped = False

        self.image = pygame.Surface((1,1))
        self.image.fill((255, 0, 0))
        self.rect = self.image.get_rect(center=self.pos)

    def update(self):

        if not self.stopped:
            self.speed -= self.decel
            if self.speed <= 0:
                self.stopped = True

        if not self.stopped:
            self.pos += self.speed * self.direction *self.master.dt
        elif self.alpha > 0:
            self.image.set_alpha(self.alpha)
            self.alpha -= self.alpha_speed *self.master.dt
        else:
            self.kill()
            return
        self.pos.y -= 0.4 *self.master.dt
        self.rect.center = self.pos +self.master.world.offset
