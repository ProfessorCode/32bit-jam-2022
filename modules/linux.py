import sys
import os
import shutil
from getpass import getuser


def add_linux_shortcut():
    entry_path = f'/home/{getuser()}/.local/share/applications/DeepAbyss.desktop'
    file_path = 'data/DeepAbyss.desktop'

    if sys.platform == 'linux':
        if not os.path.exists(entry_path):
            shutil.copy(file_path, entry_path)
            _add_data(entry_path)


def _add_data(entry_path):
    icon_path = f'{os.path.dirname(sys.executable)}/graphics/window_icon.png'

    with open(entry_path, 'a') as f:
        f.write(f'\nPath={os.path.dirname(sys.executable)}')
        f.write(f'\nExec={sys.executable}')
        f.write(f'\nIcon={icon_path}')
