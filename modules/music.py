import pygame


class Music:
    def __init__(self, master):
        self.master = master
        master.music = self
        self.tracks = {
            "main_menu": 'music/MenuTheme.ogg',
            "intro_scene": 'music/Cutscene.ogg',
            'stage1': 'music/Stage-1.ogg', 'stage2': 'music/Stage-2.ogg', 'stage3': 'music/Stage-3.ogg',
        }
        track_type = "main_menu"
        if self.master.main.state == self.master.main.IN_GAME: track_type = 'stage1'
        pygame.mixer.music.load(self.tracks[track_type])
        self.is_playing = False
        self.can_play = True
        self.started_playing = False
    
    def change_track(self, track_type):
        pygame.mixer.music.fadeout(2_000)
        pygame.mixer.music.queue(self.tracks[track_type], loops=-1)
    
    def run(self):

        self.can_play = not self.master.game.paused

        if self.can_play and not self.is_playing:
            if not self.started_playing:
                pygame.mixer.music.play(loops=-1)
                self.started_playing = True
            else:
                pygame.mixer.music.unpause()

            self.is_playing = True

        elif not self.can_play and self.is_playing:
            pygame.mixer.music.pause()
            self.is_playing = False

    def update_stage_music(self, player_stage):
        self.change_track(F"stage{player_stage+1}")
        # self.run()