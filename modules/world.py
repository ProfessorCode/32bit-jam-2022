import pygame
import csv
from .config import *
from .debug import Debug
from .engine import *


class World:
    def __init__(self, master):
        self.master = master
        self.master.world = self
        self.screen = pygame.display.get_surface()
        self.load_debugger()
        self.bounds = self.load_rects('data/bounds.csv')
        self.territories = self.load_rects('data/territories.csv')
        self.wander_areas = [area.inflate(-200, -200) for area in self.territories]
        self.offset = pygame.Vector2(-810, 65)

        self.vignette_heavy_surf = pygame.image.load("graphics/foreground/vignette_heavy.png").convert_alpha()
        self.vignette_surf = pygame.image.load("graphics/foreground/vignette.png").convert_alpha()

        #bg
        self.whole_map = pygame.image.load("graphics/map/map_done_last.png").convert_alpha()
        
        self.animated_bg = AnimatedBG(master)

    def load_debugger(self):
        self.debug = Debug()
        self.master.debug = self.debug

    def load_rects(self, filepath):
        reader = csv.DictReader(open(filepath))
        rects = []

        for rect_bounds in reader:
            left, right = int(rect_bounds['left']), int(rect_bounds['right'])
            top, bottom = int(rect_bounds['top']), int(rect_bounds['bottom'])
            width = right - left
            height = bottom - top
            rect = pygame.Rect(left, top, width, height)
            rects.append(rect)
        
        return rects

    def update_offset(self):
        # self.offset =  (self.master.player.hitbox.center - pygame.Vector2(W/2, H/2)) * -1
        camera_rigidness = 0.18 if self.master.player.moving else 0.05
        if self.master.player.dashing: camera_rigidness = 0.22
        self.offset -= (self.offset + (self.master.player.hitbox.center - pygame.Vector2(W/2, H/2))) * camera_rigidness * self.master.dt

    def draw_debug(self):
        self.debug.draw()

    def draw_background(self):
        self.screen.fill((0, 0, 0))

        # for rect in self.bounds:
        #     left, top = rect.left + self.offset.x, rect.top + self.offset.y
        #     offset_rect = (left, top, rect.width, rect.height)
        #     pygame.draw.rect(self.screen, (200, 200, 200), offset_rect)
        self.animated_bg.draw()
        self.screen.blit(self.whole_map, (-320,-256)+self.offset)
        #(64 -32) + (0, 4032) + offset

        # for i, rect in enumerate(self.territories):
        #     left, top = rect.left + self.offset.x, rect.top + self.offset.y
        #     offset_rect = (left, top, rect.width, rect.height)
        #     pygame.draw.rect(self.screen, (200, 255, 12), offset_rect, 3)

        #     num_surf = self.master.font.render(str(i), False, (200,200,200))
        #     self.screen.blit(num_surf, (left+rect.width/2 -9, top+rect.height/2 -9))

        # for rect in self.wander_areas:
        #     left, top = rect.left + self.offset.x, rect.top + self.offset.y
        #     offset_rect = (left, top, rect.width, rect.height)
        #     pygame.draw.rect(self.screen, (255, 19, 100), offset_rect, 2)

    def draw_foreground(self):
        if self.master.player.evolve_waiting:
            self.screen.blit(self.vignette_heavy_surf, (0,0))
        else: self.screen.blit(self.vignette_surf, (0,0))

    def update(self):
        self.update_offset()
        self.animated_bg.update()


class AnimatedBG:

    def __init__(self, master) -> None:
        
        self.master = master
        self.screen = pygame.display.get_surface()
        self.frames = load_pngs("graphics/map/Area-4")
        self.image = self.frames[0]
        self.frame_index = 0
        self.anim_speed = 0.15
        self.pos = (64+0, 0+4032)

    def update(self):

        try:
            image = self.frames[int(self.frame_index)]
        except IndexError:
            self.frame_index = 0
            image = self.frames[0]
        
        self.frame_index += self.anim_speed *self.master.dt

        self.image = pygame.transform.scale(image, (image.get_width()*1.2, image.get_height()*1.2))

    def draw(self):

        self.screen.blit(self.image, self.pos+self.master.world.offset)