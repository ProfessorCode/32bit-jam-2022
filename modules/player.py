import pygame
import random
from .config import *
from .engine import *
from .frect import FRect
from math import ceil
from .particles import BleedParticleManeger


class Player:

    def __init__(self, master):
        self.master = master
        self.master.player = self
        self.screen = pygame.display.get_surface()
        self.base_stats = {
            'swim_speed': 1.2,
            'sprint_speed': 1.8,
            'dash_speed': 12,
            'acceleration': .2,
            'deceleration': .3,
        }

        # self.start_pos = 770, 4175 # boss
        self.start_pos = 1130, 175
        self.boss_checkpoint_pos = 770, 4175

        self.boss_checkpoint_activated = False

        self.load_sprites()
        self.image = self.animations[0]['swim_side'][0].copy()
        self.rect = self.image.get_rect(center=self.start_pos)
        self.direction = pygame.Vector2(1, 0)
        self.angle = self.direction.angle_to((0, -1))

        self.speed = 0
        self.moving = False
        self.sprinting = False
        self.dashing = False
        self.can_sprint = False
        self.can_dash = True
        self.CAN_SPRINT_TIMER = pygame.event.custom_type()
        self.DASH_FOR = pygame.event.custom_type()
        self.DASH_COOLDOWN = pygame.event.custom_type()
        self.BUBBLES_TIMER = pygame.event.custom_type()
        self.SWIM_SOUND_TIMER = pygame.event.custom_type()
        self.INVINSIBILITY_TIMER = pygame.event.custom_type()
        self.HEAL_TIMER = pygame.event.custom_type()
        self.DEAD_TIMER = pygame.event.custom_type()
        self.DEAD_TIMER = pygame.event.custom_type()
        self.EVOLVE_DELAY = pygame.event.custom_type()
        self.EVOLVE2_DELAY = pygame.event.custom_type()
        pygame.time.set_timer(self.BUBBLES_TIMER, random.randint(5, 10)*1000, loops=1)
        pygame.time.set_timer(self.SWIM_SOUND_TIMER, 500)
        pygame.time.set_timer(self.HEAL_TIMER, 5_000)

        self.swim_speed = self.base_stats['swim_speed']
        self.sprint_speed = self.base_stats['sprint_speed']
        self.dash_speed = self.base_stats['dash_speed']
        self.acceleration = self.base_stats['acceleration']
        self.deceleration = self.base_stats['deceleration']

        self.hitbox = FRect(0, 0, 16, 16)
        self.hitbox.center = self.start_pos

        # health system
        self.bite_rect_size = 14
        self.head_offset = 15
        self.bite_rect = FRect(0, 0, self.bite_rect_size, self.bite_rect_size)
        self.bite_mask = pygame.mask.Mask((self.bite_rect_size, self.bite_rect_size), True)

        self.health = 20
        self.growth_points = 0

        self.max_health = [20, 50, 100]
        self.growth_size = [20, 99, 200]
        self.bite_size = [20, 50, 100]
        self.stage = 0
        self.update_stage(0)
        self.growth_points_needed = [95, 1100]
        self.dead = False
        self.taking_damage = False
        self.evolving = False
        self.evolve_waiting = False

        self.EVENTS = (
            pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP, self.CAN_SPRINT_TIMER,
            self.DASH_FOR, self.DASH_COOLDOWN, self.BUBBLES_TIMER, self.SWIM_SOUND_TIMER,
            self.INVINSIBILITY_TIMER, self.HEAL_TIMER, self.DEAD_TIMER, self.EVOLVE_DELAY,
            self.EVOLVE2_DELAY
        )

        self.bleed_particle_grp = pygame.sprite.Group()
        self.bleed_particle_manager = BleedParticleManeger(master, self.bleed_particle_grp)

    def load_sprites(self):

        self.animations = {0:{}, 1:{}, 2:{}}
        self.anim_frame = 0
        self.animations[0].update(import_sprite_sheets("graphics/player/stage_0/swim"))
        self.animations[0].update(import_sprite_sheets("graphics/player/stage_0/dash"))
        self.animations[0].update(import_sprite_sheets("graphics/player/stage_0/damage"))
        self.animations[0].update(import_sprite_sheets("graphics/player/stage_0/dead"))

        self.animations[1].update(import_sprite_sheets("graphics/player/stage_1/swim"))
        self.animations[1].update(import_sprite_sheets("graphics/player/stage_1/dash"))
        self.animations[1].update(import_sprite_sheets("graphics/player/stage_1/damage"))
        self.animations[1].update(import_sprite_sheets("graphics/player/stage_1/dead"))
        self.animations[0]['evolve'] = import_spritesheet("graphics/player/evolution", "form2-32x64.png")

        self.animations[2].update(import_sprite_sheets("graphics/player/stage_2/swim"))
        self.animations[2]['swim_diag'] = self.animations[2]['swim_vertical']
        self.animations[2].update(import_sprite_sheets("graphics/player/stage_2/dash_bite"))
        self.animations[2]['dash_diag'] = self.animations[2]['dash_vertical']
        self.animations[2].update(import_sprite_sheets("graphics/player/stage_2/damage"))
        self.animations[2].update(import_sprite_sheets("graphics/player/stage_2/dead"))
        self.animations[1]['evolve'] = import_spritesheet("graphics/player/evolution", "form3-64x96.png")
        pass

    def die(self, enemy):

        self.dead = True
        self.master.sounds.death.play()
        self.master.particle.spawn_blood(enemy.bite_rect.center, swallow=True)
        
        # self.growth_points -= int(self.growth_points/10 * (4 if self.stage==1 else 6)) #death panalty

        pygame.time.set_timer(self.DEAD_TIMER, 3_000, loops=1)

    def respawn(self):

        if self.boss_checkpoint_activated:
            self.hitbox.center = self.boss_checkpoint_pos
        else: self.hitbox.center = self.start_pos

        self.dead = False
        self.speed = 0
        self.health = self.max_health[self.stage]

        for enemy in self.master.enemy_grp.sprites():
            enemy.respawn()
        # self.master.game.respawn_enemies(force=True)

        self.master.sounds.bubbles_long()

    def update_stage(self, force_stage = None):

        if force_stage is None:
            self.stage += 1
        else: self.stage = force_stage

        hitbox_center = self.hitbox.center
        if self.stage == 0:
            self.swim_speed = 1.2
            self.sprint_speed = 1.8
            self.hitbox = FRect(0, 0, 16,16)
            self.bite_rect_size = 14
            self.head_offset = 15
        elif self.stage == 1:
            self.swim_speed = 1.4
            self.sprint_speed = 2.2
            self.hitbox = FRect(0, 0, 28,28)
            self.bite_rect_size = 16
            self.head_offset = 30
        elif self.stage == 2:
            self.swim_speed = 1.6
            self.sprint_speed = 2.6
            self.hitbox = FRect(0, 0, 28,28)
            self.bite_rect_size = 18
            self.head_offset = 32
            self.dash_speed = 9
            self.deceleration = 0.1

        self.master.music.update_stage_music(self.stage)

        self.hitbox.center = hitbox_center
        self.health = self.max_health[self.stage]
        self.bite_rect = FRect(0, 0, self.bite_rect_size, self.bite_rect_size)
        self.bite_mask = pygame.mask.Mask((self.bite_rect_size, self.bite_rect_size), True)


        if self.stage != 0 and force_stage is None:
            
            for enemy in self.master.game.enemy_grp.sprites():
                enemy.update_aggression_level()

            self.evolve_waiting = True
            pygame.time.set_timer(self.EVOLVE_DELAY, 2_000, loops=1)
            self.stage -= 1

    def check_growth_points(self):

        if not self.evolve_waiting and self.stage != 2 and self.growth_points >= self.growth_points_needed[self.stage]:
            self.update_stage()

    def got_bitten(self, enemy):
        
        self.dashing = False
        self.master.sounds.hurt()

        if enemy.growth_size >= self.growth_size[self.stage]:
            self.die(enemy)
            return
        self.health -= enemy.bite_size
        if self.health <= 0:
            self.die(enemy)
            return
        self.master.particle.spawn_blood(enemy.bite_rect.center)
        self.taking_damage = True
        pygame.time.set_timer(self.INVINSIBILITY_TIMER, 300, loops=1)

    def update_image(self):

        dot = self.direction.dot((0, -1))
        mirror = self.direction.dot((-1, 0)) > 0
        flip = [False, False]
        angle_offset = 0

        if dot >= 0.75:
            orientation = '_vertical'
        elif dot > 0.25:
            orientation = '_diag'
            angle_offset = -45 if self.stage == 0 else -22.5
            if mirror:
                flip[1] = True
                angle_offset = -135 if self.stage == 0 else 180+22.5
        elif dot >= -0.25:
            orientation = '_side'
            if mirror: flip[0] = True
        elif dot > -0.75:
            orientation = '_diag'
            angle_offset = -45 if self.stage == 0 else -22.5
            if mirror:
                flip[1] = True
                angle_offset = -135 if self.stage == 0 else 180+22.5
        elif dot >= -1:
            orientation = '_vertical'
        original_orientation = orientation

        if self.dead: state, orientation = "death", ""
        elif self.evolving: state, orientation = "evolve", ""
        elif self.taking_damage: state = "damage"
        elif self.dashing: state = 'dash'
        else: state = "swim"

        try:
            image = self.animations[self.stage][state+orientation][int(self.anim_frame)]
        except IndexError:
            if self.evolving:
                self.evolving = False
                self.stage += 1
                state, orientation = 'swim', original_orientation
            self.anim_frame = 0
            image = self.animations[self.stage][state+orientation][0]

        if self.taking_damage: self.anim_frame += 0.2 *self.master.dt
        elif self.dashing:
            if self.stage == 2: self.anim_frame += 0.4 *self.master.dt
            else: self.anim_frame = 0
        elif self.sprinting and self.moving: self.anim_frame += 0.26 *self.master.dt
        elif self.moving: self.anim_frame += 0.15 *self.master.dt
        elif self.evolving: self.anim_frame += 0.10 *self.master.dt
        else: self.anim_frame += 0.07 *self.master.dt

        if any(flip):
            image = pygame.transform.flip(image, *flip)

        # if self.taking_damage or self.dead or self.evolving or self.stage==2: self.angle_offset = 0
        if (self.stage == 2 and orientation == '_diag'):# or self.taking_damage or self.dead or self.evolving:
            if mirror:angle_offset = 180
            else: angle_offset = 0
        self.image = pygame.transform.rotate(image, self.angle+angle_offset)
        self.rect = self.image.get_rect(center=self.hitbox.center)

    def get_input(self):

        mouse = pygame.mouse.get_pressed()
        self.moving = mouse[0]

        if self.hitbox.collidepoint(-self.master.world.offset + pygame.mouse.get_pos()):
            self.moving = False

    def check_bound_collision(self, axis):

        if axis == 'x':

            for rect in self.master.world.bounds:
                if self.hitbox.colliderect(rect):

                    if self.direction.x > 0:
                        self.hitbox.right = rect.left
                    elif self.direction.x < 0:
                        self.hitbox.left = rect.right
                    return

        if axis == 'y':

            for rect in self.master.world.bounds:
                if self.hitbox.colliderect(rect):

                    if self.direction.y > 0:
                        self.hitbox.bottom = rect.top
                    elif self.direction.y < 0:
                        self.hitbox.top = rect.bottom
                    return

    def move(self):

        if self.dead or self.evolve_waiting: return

        if not self.dashing:
            self.direction = (-self.master.world.offset + pygame.mouse.get_pos() - self.hitbox.center).normalize()
            self.angle = self.direction.angle_to((0, -1))

            if self.moving:
                self.speed += self.acceleration
            else:
                self.speed -= self.deceleration

            if not self.sprinting and self.speed <= self.swim_speed + self.deceleration:
                self.speed = max(0, min(self.speed, self.swim_speed))
            else:
                self.speed = max(0, min(self.speed, self.sprint_speed))
        else:
            self.speed = self.dash_speed

        self.hitbox.x += self.direction.x*self.speed *self.master.dt
        self.check_bound_collision('x')
        self.hitbox.y += self.direction.y*self.speed *self.master.dt
        self.check_bound_collision('y')

        self.bite_rect.center = self.direction*self.head_offset + self.hitbox.center

    def check_bite_collision(self, enemy):

        enemy_rect = enemy.rect.move(-self.master.world.offset)
        if not enemy.taking_damage and self.bite_rect.colliderect(enemy_rect):
            
            enemy_mask = pygame.mask.from_surface(enemy.image)
            if enemy_mask.overlap(self.bite_mask, (self.bite_rect.x-enemy_rect.x, self.bite_rect.y-enemy_rect.y)) is not None:
                enemy.got_bitten()
                return True

    def check_bite(self, check_now = False):

        if self.dashing or check_now:
            for enemy in self.master.enemy_grp.sprites():
                if not enemy.active: continue
                result = self.check_bite_collision(enemy)
                if result:
                    self.master.sounds.bite()
                    self.dashing = False
                    self.health += ceil(enemy.growth_size/10)
                    self.health = min(self.health, self.max_health[self.stage])
                    return True

    def check_events(self):

        for event in pygame.event.get(self.EVENTS):
            if event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    pygame.time.set_timer(self.CAN_SPRINT_TIMER, 200, loops=1)
                    self.sprinting = False
                    self.can_sprint = True

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 3 and self.can_dash and not self.dead and not self.evolve_waiting:
                    pygame.time.set_timer(self.DASH_COOLDOWN, 1000, loops=1)
                    self.can_dash = False
                    if self.check_bite(check_now = True): continue
                    pygame.time.set_timer(self.DASH_FOR, 150, loops=1)
                    self.master.sounds.dash()
                    self.dashing = True

                elif self.can_sprint and event.button == 1:
                    self.can_sprint = False
                    self.sprinting = True
            if event.type == self.CAN_SPRINT_TIMER:
                self.can_sprint = False
            if event.type == self.DASH_FOR:
                self.master.particle.spawn_dash(self)
                self.dashing = False
                self.sprinting = True
            if event.type == self.DASH_COOLDOWN:
                self.can_dash = True
            if event.type == self.BUBBLES_TIMER:
                self.master.particle.spawn_bubbles(self.bite_rect.center)
                self.master.sounds.bubble()
                pygame.time.set_timer(self.BUBBLES_TIMER, random.randint(5, 10)*1000, loops=1)
            if event.type == self.SWIM_SOUND_TIMER and self.moving:
                self.master.sounds.swim()
            if event.type == self.INVINSIBILITY_TIMER:
                self.taking_damage = False
            if event.type == self.HEAL_TIMER and self.health < self.max_health[self.stage]:
                self.health += 1
            if event.type == self.DEAD_TIMER:
                self.respawn()
            if event.type == self.EVOLVE_DELAY:
                self.master.sounds.evolve.play()
                self.evolving = True
                self.anim_frame = 0
                pygame.time.set_timer(self.EVOLVE2_DELAY, 3_000, loops=1)
            if event.type == self.EVOLVE2_DELAY:
                self.evolve_waiting = False

    def check_checkpoint(self):
        "set checkpoint to boss when reached"
        
        if not self.boss_checkpoint_activated and self.master.world.territories[-1].collidepoint(self.hitbox.center) and self.stage == 2:
            self.boss_checkpoint_activated = True

    def debug(self):
        self.master.debug("angle: ", self.angle)
        self.master.debug("pos: ", self.hitbox.center)
        self.master.debug("sprinting: ", self.sprinting)
        self.master.debug("can dash: ", self.can_dash)
        self.master.debug("health: ", self.health)
        self.master.debug("points: ", self.growth_points)

        # rect = (self.bite_rect.x+self.master.world.offset.x, self.bite_rect.y+self.master.world.offset.y, self.bite_rect.width, self.bite_rect.height)
        # pygame.draw.rect(self.master.debug.surface, (0,0,255), rect)

    def draw(self):
        self.bleed_particle_grp.draw(self.screen)
        self.screen.blit(self.image, self.rect.topleft + self.master.world.offset)

    def update(self):
        self.get_input()
        self.check_events()
        self.move()
        self.update_image()
        self.check_bite()
        self.check_growth_points()
        self.check_checkpoint()
        self.bleed_particle_manager.update()
        self.bleed_particle_grp.update()
        # self.debug()
        