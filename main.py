import pygame
from modules import *
import sys


class Master:
    def __init__(self):
        self.font = pygame.font.Font('fonts/PressStart2P-Regular.ttf', 18)
        self.font_big = pygame.font.Font('fonts/PressStart2P-Regular.ttf', 28)
        self.font_small = pygame.font.Font('fonts/PressStart2P-Regular.ttf', 10)


class Main:

    LOGO = 0
    MAIN_MENU = 1
    IN_GAME = 2
    SCENE1 = 3

    def __init__(self):
        self.screen = pygame.display.set_mode((W, H), pygame.SCALED)
        self.clock = pygame.time.Clock()
        pygame.display.set_caption('Deep Abyss')
        pygame.display.set_icon(pygame.image.load('graphics/window_icon.png').convert())
        self.master = Master()
        self.state = self.LOGO
        self.master.main = self
        self.master.dt = 0
        SoundSet(self.master)
        self.music = Music(self.master)
        self.game = Game(self.master)
        self.main_menu = MainMenu(self.master)
        self.intro:IntroSceneFiFo = None
        self.logo = CutScene(self.master, 'logo', 0.4)
        # add_linux_shortcut()

    def run(self):

        while True:
            pygame.display.update()
            self.master.dt = self.clock.tick() / 16.667
            if self.master.dt > 12: self.master.dt = 12
            self.check_events()
            self.check_state()
            pygame.event.pump()

    def check_state(self):

        self.music.run()
        if self.state == self.LOGO:
            if self.logo.run():
                self.state = self.MAIN_MENU
                del self.logo
                self.intro = IntroSceneFiFo(self.master)
        elif self.state == self.MAIN_MENU:
            self.main_menu.run()
        elif self.state == self.IN_GAME:
            self.game.run()
        elif self.state == self.SCENE1:
            if self.intro.run():
                self.state = self.IN_GAME
                del self.intro
                self.music.change_track("stage1")

    def check_events(self):
        for event in pygame.event.get((pygame.QUIT)):
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()


def run():
    pygame.init()
    main = Main()
    main.run()


if __name__ == '__main__':
    run()
