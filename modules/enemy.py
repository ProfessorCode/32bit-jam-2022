import pygame
from .config import *
from .frect import FRect
from .engine import dist_sq
import opensimplex
from math import dist
import random
import json

enemy_stats = json.load(open("data/enemy_stats.json", 'r'))

class Enemy(pygame.sprite.Sprite):

    WANDER = 0
    FOLLOW = 1
    ATTACK = 2
    FLEE = 3
    
    def __init__(self, master, grps, pos, type, territory_index, agg_levels, sprite_type, anim_speed=0.15):

        super().__init__(grps)
        self.master = master
        self.random_y_seed = random.random()*100
        self.type = type
        my_stats = enemy_stats[type]
        self.boss = self.type == 'boss'

        self.start_pos = pos
        self.hitbox = FRect(pos[0], pos[1], my_stats["size"], my_stats["size"])
        self.velocity = pygame.Vector2(1, 0)
        self.direction = self.velocity.copy()
        self.acceleration = pygame.Vector2(0, 0)
        self.angle = self.direction.angle_to((0, -1))

        self.swim_speed = my_stats["swim_speed"]
        self.swim_turn_angle = my_stats["swim_turn_angle"]
        self.sprint_speed = my_stats["sprint_speed"]
        self.obstacle_avoidence_strength = my_stats["obstacle_avoidence_strength"]
        self.moving = True
        self.state = self.WANDER

        self.animation:list = self.master.game.enemy_animations[sprite_type]
        self.image = self.animation[0]
        self.rect = self.image.get_rect(center=self.hitbox.center+self.master.world.offset)
        self.anim_frame = 0
        self.anim_speed = anim_speed
        self.damage_surf = pygame.Surface(self.image.get_size())
        self.damage_surf.fill((255,0,0))

        self.head_offset = my_stats["head_offset"]
        self.bite_rect_size = my_stats["bite_rect_size"]
        self.bite_rect = FRect(0, 0, self.bite_rect_size, self.bite_rect_size)
        self.bite_mask = pygame.mask.Mask((self.bite_rect_size, self.bite_rect_size), True)
        #obstacle avoidence
        # self.foov = 180 # field of obejct view
        # self.vision_res = 6
        # self.vision_angles = (*[self.foov/2/(self.vision_res)*i for i in range(self.vision_res)], self.foov/2)
        self.vision_angles = (0, 15, -15, 30, -30, 45, -45, 60, -60, 75, -75, 90, -90)
        self.obstacle_detection_range = 30
        self.obstacle_detection_rect = FRect(0, 0, self.obstacle_detection_range*2, self.obstacle_detection_range*2)

        self.territory = self.master.world.territories[territory_index]
        self.wander_area = self.master.world.wander_areas[territory_index]
        self.territory_diag_dist = dist(self.territory.topleft, self.territory.center)

        #health stats
        self.health = my_stats["health"]
        self.growth_size = my_stats["growth_size"]
        self.bite_size = my_stats["bite_size"]
        self.dead = False
        self.taking_damage = False
        self.active = True

        # player detection:
        self.player_detection_range = my_stats["player_detection_range"]
        self.player_detection_fov = 0.25

        self.player_spotted = False

        self.ATTACK_EVENT = pygame.event.custom_type()
        self.ADRENALINE_TIMER = pygame.event.custom_type()
        self.WANDER_EVENT = pygame.event.custom_type()
        self.INVINCIBILITY_TIMER = pygame.event.custom_type()
        pygame.time.set_timer(self.ATTACK_EVENT, 15_000)
        self.EVENTS = (self.ATTACK_EVENT, self.ADRENALINE_TIMER, self.WANDER_EVENT, self.INVINCIBILITY_TIMER)

        self.aggression_levels = agg_levels#10, 5, 4, 3, 2, 1, 0
        self.update_aggression_level()
        if self.boss:
            self.update_aggression_level(10)


    def die(self):

        self.master.player.growth_points += self.growth_size
        self.master.particle.spawn_blood(self.master.player.bite_rect.center, swallow=True)
        if self.boss:

            self.master.particle.spawn_blood(self.hitbox.topleft, swallow=True)
            self.master.particle.spawn_blood(self.hitbox.bottomright, swallow=True)
            self.master.particle.spawn_blood(self.hitbox.topright, swallow=True)
            self.master.particle.spawn_blood(self.hitbox.bottomleft, swallow=True)
            self.master.particle.spawn_blood(self.hitbox.midtop)
            self.master.particle.spawn_blood(self.hitbox.midbottom)
            self.master.particle.spawn_blood(self.hitbox.midright)
            self.master.particle.spawn_blood(self.hitbox.midleft)
            self.master.game.boss_body.death(self)
            pygame.time.set_timer(self.master.game.END_WAIT_TIMER, 5_000, loops=1)
        self.kill()
        del self

    def respawn(self):

        self.health = enemy_stats[self.type]["health"]
        # self.hitbox.center = self.wander_area.width*random.random()+self.wander_area.left, self.wander_area.height*random.random()+self.wander_area.top
        self.hitbox.center = self.start_pos
        self.rect.center=self.hitbox.center+self.master.world.offset
        self.state = self.WANDER
        self.player_spotted = False
        self.rect = self.image.get_rect(center = (-500,-500))

    def update_aggression_level(self, force_lvl = None):


        if force_lvl is None:
            if self.boss: return
            try:
                self.aggression_level = self.aggression_levels[self.master.player.stage]
            except IndexError: return
        else: self.aggression_level = force_lvl

        if self.aggression_level == 0:
            self.state = self.WANDER
            self.player_spotted = False
        elif self.aggression_level == 1:
            pass
        elif self.aggression_level == 2:
            if self.state == self.ATTACK:
                self.state = self.FOLLOW
        elif self.aggression_level == 4:
            pygame.time.set_timer(self.ATTACK_EVENT, 10_000)
        elif self.aggression_level == 5:
            pygame.time.set_timer(self.ATTACK_EVENT, 7_000)
        elif self.aggression_level == 10:
            pygame.time.set_timer(self.ATTACK_EVENT, 20_000)
            pygame.time.set_timer(self.WANDER_EVENT, 15_000)

        if self.aggression_level < 4:
            pygame.time.set_timer(self.ATTACK_EVENT, 15_000)

    def check_bound_collision(self, axis):

        if axis == 'x':

            for rect in self.master.world.bounds:
                if self.hitbox.colliderect(rect):

                    if self.velocity.x > 0:
                        self.hitbox.right = rect.left
                    elif self.velocity.x < 0:
                        self.hitbox.left = rect.right
                    return

        if axis == 'y':

            for rect in self.master.world.bounds:
                if self.hitbox.colliderect(rect):

                    if self.velocity.y > 0:
                        self.hitbox.bottom = rect.top
                    elif self.velocity.y < 0:
                        self.hitbox.top = rect.bottom
                    return

    def check_territory_bound(self):

        if not self.territory.collidepoint(self.hitbox.center):
            if self.state == self.FOLLOW:
                if self.territory.top < self.hitbox.centery < self.territory.bottom:
                    rect_distance = self.territory.width/2
                    distance = abs(self.territory.centerx - self.hitbox.centerx) - rect_distance
                elif self.territory.left < self.hitbox.centerx < self.territory.right:
                    rect_distance = self.territory.height/2
                    distance = abs(self.territory.centery - self.hitbox.centery) - rect_distance
                else:
                    distance = dist(self.territory.center, self.hitbox.center) - self.territory_diag_dist
                center_force = (self.territory.center - self.velocity - self.hitbox.center).normalize()*0.1
                self.acceleration += center_force * max(1.2, distance / 35)

        if not self.wander_area.collidepoint(self.hitbox.center):
            if self.state == self.WANDER:
                self.acceleration += (self.territory.center - self.velocity - self.hitbox.center).normalize()*0.2

    def obstacle_avoidence(self):
        
        colliding_rects = []
        self.obstacle_detection_rect.center = self.direction*self.head_offset + self.hitbox.center
        for rect in self.master.world.bounds:
            if self.obstacle_detection_rect.colliderect(rect):
                colliding_rects.append(rect)
        if not colliding_rects: return

        for angle in self.vision_angles:
            new_direc = self.direction.rotate(angle)
            look_line = new_direc * self.obstacle_detection_range + self.hitbox.center + (self.direction*self.head_offset)
            # pygame.draw.line(self.master.debug.surface, 'orange', self.hitbox.center+self.master.world.offset+ (self.direction*self.head_offset), look_line+self.master.world.offset+ (self.direction*self.head_offset))
            for rect in colliding_rects:
                if rect.collidepoint(look_line):
                    break
            else:
                try:
                    self.acceleration += (new_direc - self.direction) * self.obstacle_avoidence_strength
                except ValueError:pass
                
    def update_acceleration(self):

        # self.acceleration = (pygame.Vector2(pygame.mouse.get_pos()) - self.master.world.offset - self.hitbox.center).normalize()
        if self.state == self.WANDER:
            self.acceleration = self.direction.rotate(opensimplex.noise2(pygame.time.get_ticks()/1_000, self.random_y_seed)*self.swim_turn_angle)
        elif self.state in (self.FOLLOW, self.ATTACK) :
            self.acceleration = (self.master.player.hitbox.center - self.velocity - self.hitbox.center).normalize() * 0.4
        elif self.state == self.FLEE:
            self.acceleration = (self.master.player.hitbox.center - self.velocity - self.hitbox.center).normalize() * -0.4
        self.obstacle_avoidence()
        self.check_territory_bound()

    def update_image(self):

        try:
            image = self.animation[int(self.anim_frame)]
        except IndexError:
            self.anim_frame = 0
            image = self.animation[0]
        
        self.anim_frame += self.anim_speed *self.master.dt
        mirror = self.direction.dot((-1, 0)) > 0
        if self.boss: mirror = not mirror

        if mirror:
            image = pygame.transform.flip(image, True, False)
        if self.boss: image = pygame.transform.scale2x(image)

        self.image = pygame.transform.rotate(image, self.angle)
        if self.taking_damage:
            self.image.fill((255,0,0), special_flags=pygame.BLEND_RGB_MULT)

        self.rect = self.image.get_rect(center = self.hitbox.center + self.master.world.offset)

    def move(self):

        if not self.moving: return
        
        self.velocity += self.acceleration* (.02 if self.aggression_level == 10 else 1) *self.master.dt
        max_speed = self.sprint_speed if self.state in (self.ATTACK, self.FLEE) else self.swim_speed
        if self.velocity.magnitude_squared() > max_speed**2:
            self.velocity.scale_to_length(max_speed)

        self.hitbox.x += self.velocity.x *self.master.dt
        self.check_bound_collision('x')
        self.hitbox.y += self.velocity.y *self.master.dt
        self.check_bound_collision('y')

        self.direction = self.velocity.normalize()
        self.angle = self.direction.angle_to((0, -1))
        self.bite_rect.center = self.hitbox.center + self.direction*self.head_offset
        
    def can_spot_player(self):
        
        if self.aggression_level == 0 or self.master.player.dead: return
        
        if self.aggression_level in (5, 10):
            if self.territory.collidepoint(self.master.player.hitbox.center):
                return True

        if self.aggression_level == 10: return

        if pygame.Vector2(self.hitbox.center).distance_squared_to(self.master.player.hitbox.center) < self.player_detection_range**2:
            if self.aggression_level == 4: return True
            dot = self.velocity.normalize().dot(pygame.Vector2(self.master.player.hitbox.centerx-self.hitbox.centerx, self.master.player.hitbox.centery-self.hitbox.centery).normalize())
            if dot > self.player_detection_fov:
                return True
    
    def got_bitten(self):

        # self.master.sounds.hurt()

        if self.growth_size <= self.master.player.growth_size[self.master.player.stage]:
            self.die()
            return

        self.health -= self.master.player.bite_size[self.master.player.stage]
        if self.health <= 0:
            self.die()
            return

        self.master.particle.spawn_blood(self.master.player.bite_rect.center)
        self.taking_damage = True
        pygame.time.set_timer(self.INVINCIBILITY_TIMER, 300, loops=1)

        if self.aggression_level == 10:
            self.state = self.FOLLOW

        if self.aggression_level == 10: return
        self.state = self.FLEE
        pygame.time.set_timer(self.ADRENALINE_TIMER, 1_500, loops=1)

    def check_bite(self):
        player = self.master.player

        if self.state in (self.FOLLOW, self.ATTACK) and not player.taking_damage:
            if self.bite_rect.colliderect(player.rect):
                player_mask = pygame.mask.from_surface(player.image)
                result = player_mask.overlap(self.bite_mask, (self.bite_rect.x-player.rect.x, self.bite_rect.y-player.rect.y)) is not None
                if result:
                    # self.master.sounds.bite()
                    player.got_bitten(self)
                    self.state = self.FLEE
                    pygame.time.set_timer(self.ADRENALINE_TIMER, 1_000, loops=1)

    def state_handler(self):
        
        if not self.player_spotted and self.can_spot_player():
            self.player_spotted = True

            if self.aggression_level in (2, 5, 10):
                self.state = self.FOLLOW
            else:
                self.state = self.ATTACK
                duration = 2_000 if self.aggression_level == 1 else 3_000
                pygame.time.set_timer(self.ADRENALINE_TIMER, duration, loops=1)

        if self.player_spotted and self.aggression_level > 1:
            if self.master.player.dead or (self.aggression_level == 10 and not self.territory.collidepoint(self.master.player.hitbox.center)):
                    self.state = self.WANDER
                    self.player_spotted = False
                    return

            distance = 600 if self.aggression_level == 2 else 800
            if dist_sq(self.hitbox.center, self.master.player.hitbox.center) > distance**2:
                self.state = self.WANDER
                self.player_spotted = False

    def check_events(self):

        for event in pygame.event.get(self.EVENTS):
            if event.type == self.ATTACK_EVENT and self.aggression_level > 2:
                if self.player_spotted and self.state not in (self.ATTACK, self.FLEE):
                    self.state = self.ATTACK
                    duration = 1_500 if self.aggression_level == 5 else 3_000
                    pygame.time.set_timer(self.ADRENALINE_TIMER, duration, loops=1)
            if event.type == self.ADRENALINE_TIMER:
                if self.player_spotted:

                    if self.aggression_level == 1:
                        self.player_spotted = False
                        if self.state == self.FLEE: self.state = self.WANDER
                        else:
                            self.state = self.FLEE
                            pygame.time.set_timer(self.ADRENALINE_TIMER, 1_000, loops=1)

                    else: self.state = self.FOLLOW
                else: self.state = self.WANDER
            if event.type == self.WANDER_EVENT and self.state != self.WANDER:
                self.state = self.WANDER
                pygame.time.set_timer(self.ADRENALINE_TIMER, 3_000, loops=1)

            if event.type == self.INVINCIBILITY_TIMER:
                self.taking_damage = False

    def update(self):

        if self.master.player.evolve_waiting:
            self.rect.center = self.master.world.offset + self.hitbox.center
            self.active = False
            return
        if dist_sq(self.master.player.hitbox.center, self.hitbox.center) > 500**2:
            if not self.boss or not self.territory.collidepoint(self.master.player.hitbox.center):
                self.active = False
                return

        self.active = True

        self.check_events()
        self.state_handler()
        self.update_acceleration()
        self.move()
        self.update_image()
        self.check_bite()
        # self.master.debug('state: ', ['wander', 'follow', 'attack', 'flee'][self.state])
        # self.master.debug('health: ', self.health)
        # this_rect = pygame.Rect(0, 0, self.player_detection_range*2, self.player_detection_range*2)
        # this_rect.center = self.obstacle_detection_rect.center + self.master.world.offset
        # pygame.draw.rect(self.master.debug.surface, 'white', self.rect, 2)


class BossEnemy(Enemy):

    def __init__(self, master, grps):

        pos = (960, 4860)
        type = 'boss'
        territory_index = -1

        super().__init__(master, grps, pos, type, territory_index, [], type, 0.3)

class BossBody:

    def __init__(self, master):

        self.master = master
        self.screen = pygame.display.get_surface()
        self.rect:pygame.Rect = None
        self.died = False
        self.image = pygame.transform.scale2x(pygame.image.load("graphics/extras/boss_death-64x96.png").convert_alpha())

    def death(self, boss:BossEnemy):

        self.image = pygame.transform.rotate(self.image, boss.angle)
        self.rect = self.image.get_rect(center=boss.hitbox.center)
        self.died = True

    def draw(self):
        if self.died:
            self.screen.blit(self.image, self.rect.topleft+self.master.world.offset)


class Kurbo(pygame.sprite.Sprite):

    def __init__(self, master, grps, pos, speed, area):

        self.master = master
        super().__init__(grps)

        self.start_pos = pos
        self.hitbox = FRect(pos[0], pos[1], 18, 18)
        self.bite_rect = self.hitbox
        self.speed = speed
        self.direction = pygame.Vector2(0, -1).rotate(random.randint(0,360))
        self.moving = True

        self.area = area
        self.can_zap = True
        self.bite_size = 50
        self.growth_size = 0

        self.animation:list = self.master.game.enemy_animations["kurbo"]
        self.image = self.animation[0]
        self.rect = self.image.get_rect(center=self.hitbox.center)
        self.anim_frame = 0
        self.anim_speed = 0.15

        self.ZAP_COOLDOWN = pygame.event.custom_type()
        self.SFX_TIMER = pygame.event.custom_type()
        pygame.time.set_timer(self.SFX_TIMER, 4_000)

        self.EVENTS = (self.ZAP_COOLDOWN, self.SFX_TIMER)

    def respawn(self):

        self.hitbox.center = self.start_pos
        self.direction = pygame.Vector2(0, -1).rotate(random.randint(0,360))

    def check_bound_collision(self, axis):

        if axis == 'x':

            for rect in self.master.world.bounds:
                if self.hitbox.colliderect(rect):

                    if self.direction.x > 0:
                        self.hitbox.right = rect.left
                    elif self.direction.x < 0:
                        self.hitbox.left = rect.right
                    self.direction.x *= -1
                    return

        if axis == 'y':

            for rect in self.master.world.bounds:
                if self.hitbox.colliderect(rect):

                    if self.direction.y > 0:
                        self.hitbox.bottom = rect.top
                    elif self.direction.y < 0:
                        self.hitbox.top = rect.bottom
                    self.direction.y *= -1
                    return

    def move(self):

        if self.moving:
            self.hitbox.x += self.direction.x * self.speed *self.master.dt
            self.check_bound_collision('x')
            self.hitbox.y += self.direction.y * self.speed *self.master.dt
            self.check_bound_collision('y')

            if self.hitbox.left < self.area.left or self.hitbox.right > self.hitbox.right:
                self.direction.x *= -1
            if self.hitbox.top < self.area.top or self.hitbox.bottom > self.hitbox.bottom:
                self.direction.y *= -1

    def check_events(self):

        for event in pygame.event.get(self.EVENTS):
            if event.type == self.ZAP_COOLDOWN:
                self.can_zap = True
            if event.type == self.SFX_TIMER and dist_sq(self.hitbox.center, self.master.player.hitbox.center) < 300**2:
                self.master.sounds.zap.play()
                pass

    def check_player_zap(self):

        if self.can_zap and self.hitbox.colliderect(self.master.player.hitbox):
            self.can_zap = False
            pygame.time.set_timer(self.ZAP_COOLDOWN, 5_000, loops=1)
            self.master.player.got_bitten(self)

    def update_image(self):

        try:
            image = self.animation[int(self.anim_frame)]
        except IndexError:
            self.anim_frame = 0
            image = self.animation[0]
        self.image = pygame.transform.scale2x(image)

        self.anim_frame += self.anim_speed *self.master.dt
        self.rect = self.image.get_rect(center = self.hitbox.center+self.master.world.offset)

    def update(self):

        self.rect.center = self.hitbox.center+self.master.world.offset
        if self.master.player.evolve_waiting or dist_sq(self.master.player.hitbox.center, self.hitbox.center) > 500**2:
            return

        self.check_events()
        self.move()
        self.check_player_zap()
        self.update_image()


class McGregor(pygame.sprite.Sprite):

    def __init__(self, master, grps, pos, side:int):

        self.master = master
        super().__init__(grps)

        self.start_pos = pos
        self.hitbox = pygame.Rect(0, 0, 150, 150)
        self.hitbox.center = pos
        self.blank_surf = pygame.Surface((1,1))
        self.blank_surf.set_alpha(0)

        self.can_slash = True
        self.bite_size = 999_999
        self.growth_size = 999_999
        self.slashing = False
        self.can_damage = True
        self.play_sound = 1

        self.other_side = False

        self.animation:list = self.master.game.enemy_animations["Mr.McGregor"]
        self.image = self.animation[0]
        self.rect = self.image.get_rect(center=self.hitbox.center+self.master.world.offset)
        self.anim_frame = 0
        self.anim_speed = 0.2

        self.side = side
        self.orientation = (side in (1,2), side in (3,2))
        self.normal_vec = pygame.Vector2(1, -1).normalize()
        if self.orientation[0]: self.normal_vec.x *= -1
        if self.orientation[1]: self.normal_vec.y *= -1
        self.bite_rect = pygame.Rect(0, 0, 48, 48)
        self.bite_rect1 = self.bite_rect.copy()
        self.bite_rect1.centerx = self.hitbox.centerx + (30 * (1 if side in (1,2) else -1))
        self.bite_rect1.centery = self.hitbox.centery
        self.bite_rect2 = self.bite_rect.copy()
        self.bite_rect2.centerx = self.hitbox.centerx
        self.bite_rect2.centery = self.hitbox.centery + (30 * (1 if side in (3,2) else -1))
        self.bite_rect = self.bite_rect1

        self.SLASH_COOLDOWN = pygame.event.custom_type()
        self.WAIT_TIMER = pygame.event.custom_type()

        self.EVENTS = (self.SLASH_COOLDOWN, self.WAIT_TIMER)

    def update_image(self):

        try:
            image = self.animation[int(self.anim_frame)]
        except IndexError:
            self.anim_frame = 0
            self.slashing = False
            pygame.time.set_timer(self.SLASH_COOLDOWN, 1_000, loops=1)
            return

        orientation = list(self.orientation)
        if self.other_side:
            image = pygame.transform.rotate(image, -90)
            orientation[0] = not orientation[0]

        self.image = pygame.transform.flip(image, *orientation)
        if self.slashing: self.anim_frame += self.anim_speed *self.master.dt

    def check_player_slash(self):

        if self.can_slash and self.master.player.hitbox.colliderect(self.hitbox):
            self.can_slash = False
            # pygame.time.set_timer(self.WAIT_TIMER, 300, loops=1)
            self.slashing = True

            if self.normal_vec.dot(pygame.Vector2(self.master.player.hitbox.centerx-self.hitbox.centerx, self.master.player.hitbox.centery-self.hitbox.centery).normalize()) > 0:
                self.other_side = True
            else: self.other_side = False

    def can_damage_player(self):

        if self.slashing and self.can_damage and 17 <= int(self.anim_frame) <= 21:
            self.bite_rect = self.bite_rect2 if self.other_side else self.bite_rect1
            if self.master.player.hitbox.colliderect(self.bite_rect):
                self.master.player.got_bitten(self)
                self.can_damage = False

    def play_sounds(self):

        if self.slashing and self.play_sound and int(self.anim_frame) in (16,):
            self.master.sounds.spiker()
            self.play_sound -= 1

    def check_events(self):

        for event in pygame.event.get(self.EVENTS):
            if event.type == self.SLASH_COOLDOWN:
                self.can_slash = True
                self.can_damage = True
                self.play_sound = 1
            if event.type == self.WAIT_TIMER:
                self.slashing = True

    def update(self):

        self.rect.center = self.hitbox.center+self.master.world.offset
        if self.master.player.evolve_waiting or dist_sq(self.master.player.hitbox.center, self.hitbox.center) > 500**2:
            return

        self.check_events()
        self.check_player_slash()
        self.can_damage_player()
        self.update_image()
        self.play_sounds()

        # rect = (self.bite_rect.x+self.master.world.offset.x, self.bite_rect.y+self.master.world.offset.y, self.bite_rect.width, self.bite_rect.height)
        # pygame.draw.rect(self.master.debug.surface, 'yellow', rect, 2)
        # pygame.draw.rect(self.master.debug.surface, 'orange', self.rect, 2)
