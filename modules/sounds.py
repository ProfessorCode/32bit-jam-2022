import pygame
from random import choice
from os import listdir

class SoundSet:

    def __init__(self, master):
        
        self.master = master
        master.sounds = self

        self.sound_path = "sounds/"

        self.bite_list = self.load_folder("sounds/bite")
        self.dash_list = self.load_folder("sounds/dash")
        self.hurt_list = self.load_folder("sounds/hurt")
        self.bubble_list = self.load_folder("sounds/bubbles")
        self.swim_list = self.load_folder("sounds/swim")
        self.start_list = self.load_folder("sounds/ui/start_game")
        self.spiker_list = self.load_folder("sounds/spiker")

        self.click = self.load_folder("sounds/ui/click")[0]
        self.hover = self.load_folder("sounds/ui/hover")[0]

        self.bubbles_long_list = self.load_folder("sounds/bubbles_long")

        self.zap = self.load("buzz/BuzzingEnemy")
        self.death = self.load("death/Death")
        self.evolve = self.load("evolve/Evolve")

    def load(self, name):

        return pygame.mixer.Sound(f"{self.sound_path}{name}.ogg")

    def load_folder(self, folder_path):

        return [pygame.mixer.Sound(f"{folder_path}/{name}") for name in listdir(folder_path) if name.endswith(".ogg")]

    def bite(self):
        choice(self.bite_list).play()

    def dash(self):
        choice(self.dash_list).play()

    def hurt(self):
        choice(self.hurt_list).play()

    def bubble(self):
        choice(self.bubble_list).play()

    def swim(self):
        choice(self.swim_list).play()

    def spiker(self):
        choice(self.spiker_list).play()

    # def click(self):
    #     choice(self.click_list).play()

    # def hover(self):
    #     choice(self.hover_list).play()

    def start_game(self):
        choice(self.start_list).play()

    def bubbles_long(self):
        choice(self.bubbles_long_list).play()