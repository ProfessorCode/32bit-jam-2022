import pygame
from .config import W, H
import json


class Prompt:
    def __init__(self, text, input_action):
        self.screen = pygame.display.get_surface()
        self.rect = pygame.rect.Rect(5, H - 25, W - 10, 20)
        self.action = input_action
        self.setup_text(text)
        self.setup_image('graphics/input')
        self.show = True
        self.current_time = 0
        self.flash_time = 0
    
    def setup_text(self, text):
        font = pygame.font.Font('fonts/PressStart2P-Regular.ttf', 12)
        self.text = font.render(text, False, 'white')
        self.text_rect = self.text.get_rect(center=self.rect.center)
    
    def setup_image(self, path):
        if not self.action == 'goal':
            self.image = pygame.image.load(f'{path}/{self.action}.png').convert_alpha()
            midright = self.text_rect.left - 5, self.text_rect.centery
            self.image_rect = self.image.get_rect(midright=midright)

    def draw(self):
        if self.current_time - self.flash_time < 500:
            rect_color = (57, 75, 240)
        else:
            rect_color = (57, 75, 50)

        pygame.draw.rect(self.screen, rect_color, self.rect)
        self.screen.blit(self.text, self.text_rect)

        if not self.action == 'goal':
            self.screen.blit(self.image, self.image_rect)
    
    def update(self):
        self.current_time = pygame.time.get_ticks()

        if not self.flash_time:
            self.flash_time = self.current_time


class PromptHandler:
    def __init__(self, master):
        self.master = master
        self.prompts = json.load(open('data/prompt_actions.json'))
        self.current_prompt = 'swim'
        self.show_prompts = True
        self.load_prompts()
        self.current_time = 0
        self.button_press_time = 0
        self.started_timer = False
    
    def load_prompts(self):
        self.swim_prompt = Prompt(
            self.prompts['swim']['text'], self.prompts['swim']['action']
        )
        self.sprint_prompt = Prompt(
            self.prompts['sprint']['text'], self.prompts['sprint']['action']
        )
        self.dash_prompt = Prompt(
            self.prompts['dash']['text'], self.prompts['dash']['action']
        )
        self.goal_prompt = Prompt(
            self.prompts['goal']['text'], self.prompts['goal']['action']
        )

    def draw(self):
        if self.show_prompts:
            prompt = self.get_prompt()
            prompt.draw()
    
    def get_prompt(self):
        if self.current_prompt == 'swim':
            prompt = self.swim_prompt
        elif self.current_prompt == 'sprint':
            prompt = self.sprint_prompt
        elif self.current_prompt == 'dash':
            prompt = self.dash_prompt
        elif self.current_prompt == 'goal':
            prompt = self.goal_prompt
        
        return prompt

    def update(self):
        if self.show_prompts:
            self.current_time = pygame.time.get_ticks()
            prompt = self.get_prompt()
            prompt.update()
            self.change_prompt(prompt)
    
    def change_prompt(self, prompt):
        show_time = self._update_countdown()

        if self.current_prompt == 'swim' and show_time > 5000:
            self.current_prompt = 'sprint'
            self.started_timer = False
        
        elif self.current_prompt == 'sprint' and show_time > 5000:
            self.current_prompt = 'dash'
            self.started_timer = False

        elif self.current_prompt == 'dash' and show_time > 5000:
            self.current_prompt = 'goal'
            self.started_timer = False
        
        elif self.current_prompt == 'goal' and show_time > 5000:
            self.show_prompts = False

    def _update_countdown(self):
        if not self.started_timer:
            self.button_press_time = pygame.time.get_ticks()
            self.started_timer = True
        
        show_time = self.current_time - self.button_press_time

        return show_time